import React from 'react';
import { Route }  from 'react-router';
import App from './App';
import Calendar from './components/Calendar';
import Clients from './components/Clients';

export default (
    <Route component={App} path='/'>
        <Route path="/cal" component={Calendar} />
        <Route path="/clients" component={Clients}/>
    </Route>
);