import { UPDATE_MEETING, DELETE_MEETING, SAVE_NEW_MEETING, SET_CURRENT_MEETING} from '../actions/actionsDate';

const INITIAL_STATE = {
    lastId: 0,
    meetingsCount: 0,
    current: {id: null},
    clients: [ 'Eddie Albert', 'Eddy Arnold', 'Neil Armstrong', 'Nils Asther', 'Stella Adler', 'Steve Allen' ]
};

const getNextId = (lastId) => {
    return lastId + 1;
};

export function meetings(state = INITIAL_STATE, action = {}) {
    switch(action.type) {
        case SET_CURRENT_MEETING:
            let newCurrent = state.current.id === action.payload.id ? INITIAL_STATE.current
                : action.payload;
            return Object.assign({}, state, {current: newCurrent});
        case UPDATE_MEETING:
            let thisDay = action.payload.date;
            return Object.assign({}, state, {[thisDay]: state[thisDay].map(meeting => {
                    if (meeting.id === action.payload.id) {
                        return action.payload;
                    }
                    return meeting
                })}
            );
        case SAVE_NEW_MEETING:
            let allMeetings = [];
            let newMeeting = action.payload;
            const day = newMeeting.date;
            newMeeting.id = getNextId(state.lastId);
            if (state[day]) {
                allMeetings = state[day];
            }
            allMeetings = [...allMeetings, newMeeting];
            return Object.assign({}, state, {lastId: newMeeting.id}, {meetingsCount: state.meetingsCount+1}, {[day]: allMeetings});
        case DELETE_MEETING:
            let currentDay = action.payload.day;
            if (state[currentDay].length < 2) {
                delete state[currentDay];
                return Object.assign({}, state, {meetingsCount: state.meetingsCount-1})
            } else {
                return Object.assign({}, state, {meetingsCount: state.meetingsCount-1}, {
                    [currentDay]: state[currentDay].filter(meeting => {
                        return meeting.id !== action.payload.id;
                    })
                });
            }
        default: return state;
    }
}