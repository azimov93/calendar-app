import React, { Component } from 'react';
import styles from './Create.scss';
import close from './assets/close.svg';
import { actions } from '../../actions/actionsDate';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Button from './Button';

class Create extends Component {
    constructor(props) {
        super(props);

        if (!this.props.form.newEntry) {
            this.state = {
                date: this.props.day,
                name: this.props.name,
                description: this.props.description,
                errors: {},
            };
        } else {
            this.state = {
                date: this.props.day,
                name: '',
                description: '',
                errors: {},
            };
        }
    };
    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
        // console.log(this.state.name);
        // console.log(this.props);
        // if (!!this.state.errors[e.target.name]) {
        //     let errors = Object.assign({}, this.state.errors);
        //     delete errors[e.target.name];
        //     this.setState({
        //         [e.target.name]: e.target.value,
        //         errors
        //     })
        // } else {
        //     this.setState({
        //         [e.target.name]: e.target.value
        //     })
        // }
    };
    handleClick = (client) => {
        this.setState({
            name: client
        })
    };
    matches = (input) => {
        let regex = new RegExp(input, 'i');
        const list = this.props.clients.filter(client => {
            return client.match(regex) && (client !== input);
        });
        return list
    };
    handleSubmit = (e) => {
        e.preventDefault();
        let meeting;
        if (this.props.id === null) {
            meeting = {
                date: this.state.date,
                id: this.props.noNullID + 1,
                name: this.state.name,
                description: this.state.description
            };
        } else {
            meeting = {
                date: this.state.date,
                id: this.props.id,
                name: this.state.name,
                description: this.state.description
            };
        }
        let errors = {};
        if (meeting.name.length < 1 || meeting.description.length < 1) {
            if (meeting.name.length < 1) errors.name = 'Participant is required';
            if (meeting.description.length < 1) errors.description = 'Description is required';
        }
        this.setState({
            errors
        });
        const isValid = Object.keys(errors).length === 0;

        if (isValid) {
            if (!this.props.form.newEntry) {
                this.props.actions.updateMeeting(meeting);
                this.props.actions.toggleForm();
            } else {
                this.props.actions.saveNewMeeting(meeting);
                this.props.actions.toggleForm();
            }
        }
    };
    render() {
        return (
            <div id='form' className={styles.wrap}>
                <div className={styles.header}>
                    <h1 className={styles.title}>
                        { this.props.form.newEntry ? `New meeting on ${this.props.selected}` : `Edit meeting on ${this.props.selected}`}
                    </h1>
                    <img className={styles.close} src={close} onClick={this.props.actions.toggleForm} alt="close"/>
                </div>
                <div className={styles.layout}>
                    <form className={styles.form} onSubmit={this.handleSubmit}>
                        <div className={styles.line}>
                            <label className={styles.label}>
                                Participant
                            </label>
                            <input
                                className={`${styles.input}` + (this.state.errors.name ? ` ${styles.error}` : ``)}
                                name='name'
                                value={this.state.name}
                                onChange={this.handleChange}
                                placeholder='Name'
                                autoComplete='off'
                            />
                            { this.state.name && !!this.matches(this.state.name).length && <div className={styles.block}>
                                {   this.matches(this.state.name).map(client => {
                                        return <Button key={client} handleClick={this.handleClick} client={client} />
                                    })
                                }
                            </div> }
                            { this.state.errors.name && <span className={styles.errorMessage}>{this.state.errors.name}</span> }
                        </div>
                        <div className={styles.line}>
                            <label className={styles.label}>
                                Description
                            </label>
                            <textarea
                                className={`${styles.message}` + (this.state.errors.description ? ` ${styles.error}` : ``)}
                                name='description'
                                value={this.state.description}
                                onChange={this.handleChange}
                                placeholder='Meeting description' />
                            { this.state.errors.description && <span className={styles.errorMessage}>{this.state.errors.description}</span> }
                        </div>
                        <div className={styles.buttons}>
                            <button
                                onClick={this.props.actions.toggleForm}
                                name='cancel'
                                className={`${styles.button} ${styles.cancel}`}
                            >
                                CANCEL
                            </button>
                            <button
                                type='submit'
                                name='save'
                                className={`${styles.button} ${styles.save}`}
                            >
                                SAVE
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        )
    };
}

const mapStateToProps = (state) => {
    return {
        date: state.meetings.current.date,
        id: state.meetings.current.id,
        noNullID: state.meetings.lastId,
        newEntry: state.form.newEntry,
        name: state.meetings.current.name,
        description: state.meetings.current.description,
        form: state.form,
        clients: state.meetings.clients
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Create);