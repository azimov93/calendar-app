import React, { Component } from 'react';
import styles from './Create.scss';

class Button extends Component {
    onClick = () => {
        return this.props.handleClick(this.props.client)
    };
    render() {
        return (
            <button className={styles.autocomplete} onClick={this.onClick}>
                {this.props.client}
            </button>
        )
    }
}

export default Button