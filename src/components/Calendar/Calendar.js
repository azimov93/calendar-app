import React, { Component } from 'react';
import styles from './Calendar.scss';
import DayNames from './DayNames';
import Week from './Week';
import arrow from './assets/arrow.svg';
import moment from 'moment';
import { actions } from '../../actions/actionsDate';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Date from '../Date';

class Calendar extends Component {
    state =  {
        month: moment().startOf("day").clone(),
        selected: moment().startOf("day"),
    };
    previous = () => {
        const month = this.state.month;
        month.add(-1, "M");
        this.setState({ month: month });
    };
    next = () => {
        const month = this.state.month;
        month.add(1, "M");
        this.setState({ month: month });
    };
    select = (day) => {
        this.setState({
                selected: day.date
        });
    };
    render() {
        return (
            <div className={styles.wrap}>
                <div className={styles.calendar}>
                    <div className={styles.header}>
                        <img className={`${styles.arrow} ${styles.prev}`} src={arrow} onClick={this.previous} alt="prev"/>
                        {this.renderMonthLabel()}
                        <img className={`${styles.arrow} ${styles.next}`} src={arrow} onClick={this.next} alt="prev"/>
                    </div>
                    <DayNames />
                    {this.renderWeeks()}
                </div>
                <Date selected={this.state.selected} date={this.state.selected.format('DDMMMYY')}/>
            </div>
        );
    }
    renderWeeks = () => {
        let weeks = [],
            done = false,
            date = this.state.month.clone().startOf("month").add("w"-1).startOf('isoWeek'),
            monthIndex = date.month(),
            count = 0;


        while (!done) {
            weeks.push(<Week key={date.toString()} date={date.clone()} month={this.state.month} select={this.select} selected={this.state.selected} planned={this.props.meetings} />);
            date.add(1, "w");
            done = count++ > 2 && monthIndex !== date.month();
            monthIndex = date.month();
        }

        return weeks;
    };
    renderMonthLabel = () => {
        return <span className={styles.label}>{this.state.month.format("MMM YYYY")}</span>;
    }
}

const mapStateToProps = (state) => {
    return {
        meetings: state.meetings,
        current: state.meetings.current,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Calendar);