import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { browserHistory, Router } from 'react-router';
import routes from './routes';
import persistState from 'redux-localstorage';
import { combineReducers } from 'redux';
import { meetings } from './reducers/meetings';
import { form } from './reducers/form';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
    meetings: meetings,
    form: form
});

const INITIAL_STATE = localStorage.getItem('redux') ? JSON.parse(localStorage.getItem('redux')) : {};

const store = createStore(
    rootReducer,
    INITIAL_STATE,
    composeEnhancers(applyMiddleware(thunk), persistState('meetings'))
);

ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory} routes={routes} />
    </Provider>,
  document.getElementById('app')
);
